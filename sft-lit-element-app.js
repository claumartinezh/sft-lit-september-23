import { LitElement, html, css } from 'lit-element';

// Importamos los componentes que se van a renderizar
import './app/components/lit-hello-name';
import './app/components/lit-counter';
import './app/components/lit-address';
import './app/components/lit-contact-list';
import './app/components/lit-contact-list-api';
import './app/components/lit-contact-card';
import './app/components/lit-agenda';
import './app/components/lit-playlist';
import './app/components/lit-song';
import './app/components/lit-splayer';

// Import resources
import locationMock from './resources/mocks/location.js';
import contactsListMock from './resources/mocks/contacts.js';
import songMock from './resources/mocks/song.js';

export class SftLitElementApp extends LitElement {
	static get properties() {
		return {};
	}

	constructor() {
		super();
	}

	static get styles() {
		return css`
			:host {
				display: block;
				box-sizing: border-box;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			h1 {
				padding-top: 20px;
				margin-top: 20px;
				border-top: 2px solid #1973b8;
				font-size: 28px;
				color: #1973b8;
			}

      .to-do {
				border-top: 2px solid #db7e14;
        color: #db7e14;
      }
		`;
	}

	render() {
		return html`
			${this.exercise1()}
			${this.exercise2()}
			${this.exercise3()}
			${this.exercise4_1()}
			${this.exercise4_2()}
			${this.exercise4_3()}
			${this.exercise4_4()}
			${this.exercise5_1()}
			${this.exercise5_2()}
			${this.exercise5_3()}
		`;
	}

  exercise1() {
    return html`
      <h1>Ejercicio 1: hello name</h1>
			Crear un componente que muestre un saludo y pueda modificar el nombre
			mediante un atributo.

			<h2>Componente por defecto:</h2>
			<!-- Instanciamos al componente lit-hello-name -->
			<lit-hello-name></lit-hello-name>

			<h2>Componente con name por atributo:</h2>
			<!-- Al pasarle un valor a la propiedad name desde fuera del componente, el valor por defecto se reemplaza por el nuevo valor -->
			<lit-hello-name name="Claudia"></lit-hello-name>
    `;
  }

  exercise2() {
    return html`
      <h1>Ejercicio 2: Contador clicks</h1>
			Crear un componente que muestre un botón y un texto que muestre el número
			de clicks realizados.
			<h2>Haz click</h2>
			<lit-counter></lit-counter>
    `;
  }

  exercise3() {
    return html`
      <h1>Ejercicio 3: lit-address</h1>
			Crear un componente que:
			<ul>
				<li>
					1) Reciba un objeto location: { street: { number, name }, city, state,
					country }
				</li>
				<li>
					2) Despliegue esos datos y con un botón actualice el número de la
					calle (Mediante un input o generando un número aleatorio)
				</li>
			</ul>
			<lit-address .location=${locationMock}></lit-address>
    `;
  }

  exercise4_1(className) {
    return html`
      <h1 class=${className}>Ejercicio 4.1: Contacts list</h1>
			Crear un componente que reciba un array: [{name: 'Contact One'}, {…}] que:
			<ul>
				<li>1) Despliegue la lista</li>
				<li>
					2) Incluya un input y un botón para agregar nuevos contactos a la
					lista
				</li>
				<li>3) Incluya un botón para borrar el último contacto de la lista</li>
				<li>4) Incluya un botón para hacer shuffle de los contactos</li>
			</ul>

			<h2>Contacts</h2>
			<lit-contact-list .contactsList="${contactsListMock}"></lit-contact-list>
    `;
  }

  exercise4_2(className) {
    return html`
      <h1 class=${className}>Ejercicio 4.2: Contacts list from an API</h1>
			<h2>Contacts from API</h2>
			<ul>
				<li>
					1) Modificar el componente 'lit-contact-list' para obtener el listado
					de usuarios desde la API https://jsonplaceholder.typicode.com/users
				</li>
				<li>2) Añadir estilos al componente</li>
			</ul>
      <lit-contact-list-api></lit-contact-list-api>
    `;
  }

  exercise4_3(className) {
    return html`
      <h1 class=${className}>Ejercicio 4.3: lit-contact-card</h1>
			<h2>Contact card</h2>
			<p>
				Crear un componente que reciba un objeto: {name, username, email,
				address: {street, suite, city, zipcode}} y despliegue esos datos
			</p>
      <lit-contact-card .contactInfo=${contactsListMock[0]}></lit-contact-card>
    `;
  }

  exercise4_4(className) {
    return html`
    <h1 class=${className}>Ejercicio 4.4: lit-agenda</h1>
    <p>
      Crear un componente que haga de comunicador entre 'lit-contact-list-api'
      y 'lit-contact-card'.
    </p>
    <p>
      Al dar click sobre un contacto, 'lit-contact-card' debe actualizarse
    </p>
    <lit-agenda></lit-agenda>
  `;
  }

  exercise5_1(className) {
    return html`
      <h1 class=${className}>Ejercicio 5.1: lit-playlist</h1>
			<p>
				Crear un componente que obtenga un listado de canciones desde la API:
				https://my-json-server.typicode.com/claumartinezh/songs-db/songs
			</p>
			<ul>
				<li>1) Debe mostrar una lista con esas canciones.</li>
				<li>
					2) Debe incluir un select para ordenar la lista por título, artista,
					duración o ranking
				</li>
				<li>3) La lista debe poder ser filtrada mediante un input</li>
			</ul>
      <lit-playlist></lit-playlist>
    `;
  }

  exercise5_2(className) {
    return html`
      <h1 class=${className}>Ejercicio 5.2: lit-song</h1>
			<p>
				Crear un componente que dado un objeto { rank, title, artist, album,
				year, duration } muestre el título, artista, duración y "reproductor"
			</p>
			<lit-song .songInfo=${songMock}></lit-song>
    `;
  }

  exercise5_3(className) {
    return html`
      <h1 class=${className}>Ejercicio 5.3: lit-splayer</h1>
			<p>
				Crear un componente 'lit-splayer' que muestre el componente
				'lit-playlist'
			</p>
			<ul>
				<li>
					Al pinchar una canción, se oculta 'lit-playlist' y se muestra
					'lit-song'
				</li>
				<li>
					Al pinchar en el botón atrás (<) se oculta 'lit-song' y se vuelve a
					mostrar 'lit-playlist'
				</li>
			</ul>
      <lit-splayer></lit-splayer>
    `;
  }
}

customElements.define('sft-lit-element-app', SftLitElementApp);
