import { css } from 'lit-element';

export const sharedStyles = css`
:host {
  font-family: 'BentonSans Regular', sans-serif;
}
.button-default {
  background-color: #1973b8;
  color: #FFFFFF;
  border: none;
  padding: 16px 32px;
  font-size: 15px;
}

.button-default:hover {
  cursor: pointer;
}

.input-default {
  background-color: #f4f4f4;
  border: none;
  border-bottom: 1px solid #666666;
  padding: 16px;
}
`;