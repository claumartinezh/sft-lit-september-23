import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';
export class LitPlaylist extends LitElement {
	static get properties() {
		return {
			songsList: { type: Array },
			visibleList: { type: Array },
			searchedSong: { type: String },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			.playlist-container {
				background-color: lavender;
				padding: 10px;
				width: 600px;
			}
			.searcher {
				display: flex;
				align-items: center;
				justify-content: space-between;
				font-size: 18px;
			}
			h1 {
				font-size: 20px;
			}
			input,
			button,
			select {
				padding: 10px;
			}
			.list {
				background-color: lavender;
				padding: 0;
				height: 600px;
			}
			.song {
				height: 60px;
				font-size: 12px;
				display: flex;
				align-items: center;
				justify-content: space-between;
				padding: 0 20px;
			}
			.song:hover {
				background-color: lavenderblush;
			}
			.text {
				width: 70%;
				display: flex;
				flex-direction: column;
			}
			.title {
				font-weight: bold;
				font-size: 14px;
				margin: 5px 0;
			}
			.play-icon {
				width: 32px;
				height: 32px;
			}
		`];
	}

	constructor() {
		/**
		 * Ejercicio 5.1
		 * Crear un componente que dado un array de objetos muestre una lista con esas canciones
		 * 1) Debe incluir un select para ordenar la lista por título, artista, duración o ranking
		 * 2) La lista debe poder ser filtrada mediante un input
		 */
		super();
		this.searchedSong = '';
		this.visibleList = [];
		this.icon = '';
	}

	connectedCallback() {
		super.connectedCallback();
		this.getSongs();
	}

	disconnectedCallback() {
		super.disconnectedCallback();
	}

	firstUpdated() {
		this.orderList('title');
	}

	getSongs() {
		fetch('https://my-json-server.typicode.com/claumartinezh/songs-db/songs')
			.then((response) => response.json())
			.then((data) => {
				this.songsList = [...data];
				this.visibleList = [...this.songsList];
			})
			.catch((err) => {
				console.log('There was an error on fetch', err);
			});
	}

	render() {
		return html`
			<div class="playlist-container">
				<div class="searcher">
					<h1>My playlist</h1>
					<div>
						<input
							name="searcher"
							size="20"
							@input="${this.handleInputChange}"
							.value="${this.searchedSong}"
						/>
						<button @click="${() => this.searchSong(this.searchedSong)}">
							Search
						</button>
					</div>
				</div>
				<select @change="${(val) => this.orderList(val.currentTarget.value)}">
					<option value="title">Order by Title</option>
					<option value="artist">Order by Artist</option>
					<option value="duration">Order by Duration</option>
					<option value="rank">Order by Rank</option>
				</select>
				<ul class="list">
					${this.visibleList.map((e) => {
						return html` <li
							class="song"
							@click="${() => this.handleSongClick(e)}"
						>
							<span>${e.rank}</span>
							<svg
								class="play-icon"
								version="1.1"
								xmlns="http://www.w3.org/2000/svg"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								x="0px"
								y="0px"
								viewBox="0 0 1000 1000"
								enable-background="new 0 0 1000 1000"
								xml:space="preserve"
							>
								<metadata>
									Svg Vector Icons : http://www.onlinewebfonts.com/icon
								</metadata>
								<g>
									<g
										transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
									>
										<path
											d="M4526.2,5008.1c-1477.3-151.4-2791.8-942.7-3625.3-2186.3C594.3,2362,306.9,1678,197.7,1139.5C115.3,733.3,100,574.3,100,124c0-450.3,15.3-605.5,97.7-1015.5C305-1422.3,592.4-2108.3,900.9-2573.9c921.6-1377.7,2458.4-2207.3,4094.7-2207.3c1850.9,0,3556.3,1059.6,4386,2722.8c831.6,1668.9,653.4,3631-467.5,5123.7c-776,1034.7-1921.8,1714.9-3228.6,1916.1C5445.9,5017.7,4771.4,5035,4526.2,5008.1z M5380.8,4243.6c310.4-30.7,595.9-92,906.3-193.5c1247.4-408.1,2230.3-1393,2638.5-2642.3c484.8-1485,97.7-3100.3-1007.9-4205.8c-873.7-873.7-2100-1316.4-3316.8-1195.7C2627.3-3798.3,1073.4-2242.4,876-261.1c-130.3,1316.4,425.4,2682.6,1437.1,3533.3C3175.3,3996.5,4294.3,4350.9,5380.8,4243.6z"
										/>
										<path
											d="M3754,2203c-42.2-19.2-74.7-53.7-99.6-109.2C3618,2013.3,3616,1952,3616,124c0-1829.9,1.9-1889.3,38.3-1969.7c49.8-109.2,143.7-153.3,260.6-122.6c113,30.7,3199.9,1831.8,3293.8,1923.8c69,67.1,76.7,82.4,76.7,168.6c0,86.2-7.7,101.6-76.7,168.6c-78.5,76.7-3140.5,1872-3265,1916.1C3861.3,2237.5,3832.6,2237.5,3754,2203z"
										/>
									</g>
								</g>
							</svg>
							<div class="text">
								<span>${e.artist}</span>
								<span class="title">${e.title}</span>
							</div>
							${e.duration}
						</li>`;
					})}
				</ul>
			</div>
		`;
	}

	/**
	 * Orders list alphabetically
	 * @param {Array} list list of songs
	 * @returns ordered list
	 */
	orderList(param) {
		this.visibleList = [
			...this.visibleList.sort((a, b) => {
				if (a[param] > b[param]) {
					return 1;
				}
				if (a[param] < b[param]) {
					return -1;
				}
				return 0;
			}),
		];
	}

	/**
	 * Updates song title to search
	 * @param {String} val input value
	 */
	handleInputChange(val) {
		this.searchedSong = val.srcElement.value;
		this.searchSong(this.searchedSong);
	}

	/**
	 * Searches in playlist a title with given string
	 * @param {String} song title of a song
	 */
	searchSong(song) {
		this.visibleList = [
			...this.songsList.filter((e) =>
				e.title.toLowerCase().includes(song.toLowerCase())
			),
		];
	}

	handleSongClick(song) {
		this.dispatchEvent(
			new CustomEvent('song-clicked', {
				detail: song,
				composed: true,
				bubbles: true,
			})
		);
	}
}
customElements.define('lit-playlist', LitPlaylist);
