import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';
import './lit-contact-list-api';
import './lit-contact-card';

export class LitAgenda extends LitElement {
	static get properties() {
		return {
			selectedContact: { type: Object },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			:host {
				display: block;
				box-sizing: border-box;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			.agenda-contact {
				display: flex;
				justify-content: space-between;
        max-width: 1200px;
			}

			h2 {
				color: #2dcccd;
			}
		`];
	}

	constructor() {
		/**
		 * Ejercicio 4.4
		 * Desplegar ambos componentes <lit-contact-list-api> y <lit-contact-card>
		 * Al dar click sobre un contacto, <lit-contact-card> debe actualizarse
		 *
		 * Habrá que hacer alguna modificación en el componente list para poder obtener el evento click de los items
		 */
		super();
	}

	render() {
		return html`
			<h2>My agenda</h2>
			<div class="agenda-contact">

				<!-- Escuchamos el evento que lanza el componente al hacer click en un contacto para obtener esos datos. -->
				<lit-contact-list-api
					@contact-clicked=${this.handleContactClicked}
				></lit-contact-list-api>

				<!-- Comprobamos que haya contacto seleccionado para renderizar el componente contact-card. En el caso de que no haya valor, no se mostrará nada en este lado -->
				${this.selectedContact
					? html`
							<lit-contact-card
								.contactInfo=${this.selectedContact}
							></lit-contact-card>
					  `
					: ''}
			</div>
		`;
	}

	/**
	 * Updates selected contact with clicked user info
	 * @param {Event} ev event from lit-contact-list-api
	 */
	handleContactClicked(ev) {
		// Obtenemos los datos desde el campo detail del evento.
		this.selectedContact = ev.detail;
	}
}

customElements.define('lit-agenda', LitAgenda);
