import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';
export class LitSong extends LitElement {
	static get properties() {
		return {
			songInfo: { type: Object },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			.song-container {
				background-color: lavender;
				display: flex;
				flex-direction: column;
				align-items: center;
				gap: 20px;
				font-size: 12px;
				padding: 20px;
				text-align: center;
			}
			.info {
				display: flex;
				flex-direction: column;
				gap: 5px;
			}
			.title {
				font-weight: bold;
				font-size: 14px;
			}
			.options {
				width: 100%;
			}
			.time-bar {
				display: flex;
				justify-content: space-between;
				align-items: center;
				gap: 10px;
			}
			.bar {
				background-color: #000000;
				width: 100%;
				height: 2px;
			}
			.song-logo {
				transform: scale(2);
				width: 50px;
				height: 50px;
				margin: 30px 0;
			}
		`];
	}

	constructor() {
		/**
		 * Ejercicio 5.2
		 * Crear un componente lit-song que dado un objeto
		 * { rank, title, artist, album, year, duration }
		 * muestre el título, artista, duración y "reproductor"
		 */
		super();
	}

	render() {
		return html`
			<div class="song-container">
				<!-- song icon -->
				<svg class="song-logo" xmlns="http://www.w3.org/2000/svg">
					<title>A music note icon</title>
					<path
						d="M19.65 42q-3.15 0-5.325-2.175Q12.15 37.65 12.15 34.5q0-3.15 2.175-5.325Q16.5 27 19.65 27q1.4 0 2.525.4t1.975 1.1V6h11.7v6.75h-8.7V34.5q0 3.15-2.175 5.325Q22.8 42 19.65 42Z"
					/>
				</svg>
				<div class="info">
					<span class="title">${this.songInfo.title}</span>
					<span class="artist">${this.songInfo.artist}</span>
				</div>
				<div class="options">
					<div>
						<!-- prev icon -->
						<svg xmlns="http://www.w3.org/2000/svg" height="48" width="48">
							<title>A 'previous sign' icon</title>
							<path
								d="M11 36V12h3v24Zm26 0L19.7 24 37 12Zm-3-12Zm0 6.25v-12.5L24.95 24Z"
							/>
						</svg>
						<!-- play icon -->
						<svg
							width="48"
							version="1.1"
							xmlns="http://www.w3.org/2000/svg"
							xmlns:xlink="http://www.w3.org/1999/xlink"
							x="0px"
							y="0px"
							viewBox="0 0 1000 1000"
							enable-background="new 0 0 1000 1000"
							xml:space="preserve"
						>
							<title>A 'play sign' icon</title>
							<g>
								<g
									transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
								>
									<path
										d="M4526.2,5008.1c-1477.3-151.4-2791.8-942.7-3625.3-2186.3C594.3,2362,306.9,1678,197.7,1139.5C115.3,733.3,100,574.3,100,124c0-450.3,15.3-605.5,97.7-1015.5C305-1422.3,592.4-2108.3,900.9-2573.9c921.6-1377.7,2458.4-2207.3,4094.7-2207.3c1850.9,0,3556.3,1059.6,4386,2722.8c831.6,1668.9,653.4,3631-467.5,5123.7c-776,1034.7-1921.8,1714.9-3228.6,1916.1C5445.9,5017.7,4771.4,5035,4526.2,5008.1z M5380.8,4243.6c310.4-30.7,595.9-92,906.3-193.5c1247.4-408.1,2230.3-1393,2638.5-2642.3c484.8-1485,97.7-3100.3-1007.9-4205.8c-873.7-873.7-2100-1316.4-3316.8-1195.7C2627.3-3798.3,1073.4-2242.4,876-261.1c-130.3,1316.4,425.4,2682.6,1437.1,3533.3C3175.3,3996.5,4294.3,4350.9,5380.8,4243.6z"
									/>
									<path
										d="M3754,2203c-42.2-19.2-74.7-53.7-99.6-109.2C3618,2013.3,3616,1952,3616,124c0-1829.9,1.9-1889.3,38.3-1969.7c49.8-109.2,143.7-153.3,260.6-122.6c113,30.7,3199.9,1831.8,3293.8,1923.8c69,67.1,76.7,82.4,76.7,168.6c0,86.2-7.7,101.6-76.7,168.6c-78.5,76.7-3140.5,1872-3265,1916.1C3861.3,2237.5,3832.6,2237.5,3754,2203z"
									/>
								</g>
							</g>
						</svg>
						<!-- next icon -->
						<svg xmlns="http://www.w3.org/2000/svg" height="48" width="48">
							<title>A 'next sign' icon</title>
							<path
								d="M34 36V12h3v24Zm-23 0V12l17.3 12Zm3-12Zm0 6.25L23.05 24 14 17.75Z"
							/>
						</svg>
					</div>
					<div class="time-bar">
						0:00
						<div class="bar"></div>
						${this.songInfo.duration}
					</div>
				</div>
			</div>
		`;
	}
}

customElements.define('lit-song', LitSong);
