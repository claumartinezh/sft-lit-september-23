import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';
import './lit-song';
import './lit-playlist';

export class LitSplayer extends LitElement {
	static get properties() {
		return {
			selectedSong: { type: Object },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			*,
			*:before,
			*:after {
				background-color: lavender;
				padding: 10px;
			}
			.splayer {
				display: flex;
				height: 800px;
				gap: 50px;
				width: 650px;
			}
			.song-detail {
				width: 100%;
			}
			.back-button {
				font-weight: bold;
				border: none;
				font-size: 14px;
				margin-bottom: 100px;
			}
		`];
	}

	constructor() {
		/**
		 * Ejercicio 5.3
		 * Crear un componente lit-splayer que muestre el componente <lit-playlist>
		 * Al pinchar una canción, se oculta <lit-playlist> y se muestra <lit-song>
		 * Al pinchar en el botón atrás (<) se oculta <lit-song> y se vuelve a mostrar <lit-playlist>
		 */
		super();
    this.selectedSong = {};
	}

	render() {
		return html`
			<div class="splayer">
				${Object.keys(this.selectedSong).length     // Comprueba si el objeto tiene alguna key
					? html`
							<div class="song-detail">
								<button class="back-button" @click="${this.goBack}">
									< My playlist
								</button>
								<lit-song .songInfo="${this.selectedSong}"></lit-song>
							</div>
					  `
					: html`
							<lit-playlist
								@song-clicked="${this.handleClickedSong}"
							></lit-playlist>
					  `}
			</div>
		`;
	}

	/**
	 * Updates selected song info
	 * @param {Event} ev event from lit-playlist
	 */
	handleClickedSong(ev) {
		this.selectedSong = ev.detail;
	}

	/**
	 * Unselects song and displays the list
	 */
	goBack() {
		this.selectedSong = {};
	}
}

customElements.define('lit-splayer', LitSplayer);
