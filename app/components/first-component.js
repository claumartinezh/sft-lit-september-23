import { LitElement, html, css } from 'lit-element';

export class FirstComponent extends LitElement {

  static get properties() {
    return {
      prop: { type: String }
    };
  }

  constructor() {
    super();
    this.prop = 'El primer componente';
  }

  static get styles() {
    return css`
      :host {
        display: block;
        box-sizing: border-box;
      }

      *, *:before, *:after {
        box-sizing: inherit;
      }
    `;
  }

  render() {
    return html`
      ${this.prop}
    `;
  }

}

customElements.define('first-component', FirstComponent);