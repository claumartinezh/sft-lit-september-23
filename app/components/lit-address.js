import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';

export class LitAddress extends LitElement {
	static get properties() {
		return {
			location: { type: Object },
			inputNumber: { type: Number },
		};
	}

	constructor() {
		/**
		 * Ejercicio 3
		 * Crear un componente que:
		 * 1) Reciba un objeto location: {
		 *      street: {
		 *        number,
		 *        name
		 *      },
		 *      city,
		 *      state,
		 *      country
		 *   }
		 *
		 * 2) Despliegue esos datos y con un botón actualice el número de la calle
		 * (Mediante un input o generando un número aleatorio)
		 *
		 */

		super();
		this.location = {
			street: {
				number: null,
				name: '',
			},
			city: '',
			state: '',
			country: '',
		};
		this.inputNumber = null;
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			:host {
				display: block;
				box-sizing: border-box;
				color: grey;
				font-size: 20px;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			.name {
				color: lightblue;
			}
		`];
	}

	render() {
		return html`
			<p>
				Street: ${this.location.street.name}, ${this.location.street.number}
			</p>
			<input
        class="input-default"
				name="street-number"
				type="number"
				@change="${this.updateInputValue}"
				.value="${this.inputNumber}"
			/>
			<button class="button-default" @click="${this.generateInputNumber}">
				Change number via input
			</button>
			<button class="button-default" @click="${this.generateRandomNumber}">
				Change to random number
			</button>
			<p>City: ${this.location.city}</p>
			<p>State: ${this.location.state}</p>
			<p>Country: ${this.location.country}</p>
		`;
	}

	updateInputValue(e) {
		// Hace falta utilizar el evento 'change' del input html nativo para obtener el valor introducido en el input.
		this.inputNumber = e.srcElement.value;
	}

	generateInputNumber() {
		this.location.street.number = this.inputNumber;
		this.inputNumber = null; // Limpiamos el valor del input
	}

	generateRandomNumber() {
    const randomNumber = parseInt(Math.random() * 100);

    // ---- OPCION 1 ----
    /**
     * Utilizar el spread operator para que se detecte el cambio en el objeto.
     */
     this.location = {
      ...this.location,
      street: {
        ...this.location.street,
        number: randomNumber
      },
    };

    // ---- OPCION 2 ----
    /**
     * Utilizar Object.assign() para que se detecte el cambio en el objeto.
     */
    // this.location = Object.assign({}, this.location, {
    //   street: Object.assign({}, this.location.street, {
    //     number: randomNumber,
    //   }),
    // });

    // ---- OPCION 3 ----
    /**
     * Utilizar requestUpdate() para que vuelva a hacer un renderizado y detecte el cambio.
     * Esto ocurre porque al ser Objeto LitElement no detecta el cambio de manera automática.
     */
    // this.location.street.number = randomNumber;
		// this.requestUpdate();
	}
}

customElements.define('lit-address', LitAddress);
