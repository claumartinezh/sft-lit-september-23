import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';

export class LitContactCard extends LitElement {
	static get properties() {
		return {
			contactInfo: { type: Object },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			:host {
				display: block;
				box-sizing: border-box;
				font-size: 15px;
				width: 380px;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			.main-info {
				background-color: #5ac4c4;
				padding: 18px;
				color: #ffffff;
			}

			.contact-text {
				font-weight: bold;
				color: #000000;
				font-size: 22px;
			}

			.secondary-info {
				padding: 18px;
			}

			.legend {
				color: #1464a5;
				font-weight: bold;
				margin-top: 20px;
			}

			.card-container {
				box-shadow: 0 1px 3px 0 rgba(18, 18, 18, 0.2);
			}
		`];
	}

	constructor() {
		/**
		 * Ejercicio 4.3
		 * Crear un componente tipo card que:
		 * 1) Reciba un objeto tipo {name, username, email, address: {street, suite, city, zipcode}}
		 * 2) Despliegue esos datos
		 */
		super();
	}

	render() {
		return html`
			<div class="card-container">
				<div class="main-info">
					<p>Hello, my name is</p>
					<p class="contact-text">${this.contactInfo?.name}</p>
				</div>
				<div class="secondary-info">
					<div>
						<span class="legend">Username</span>
						<p>${this.contactInfo?.username}</p>
					</div>
					<div>
						<p class="legend">Email</p>
						<p>${this.contactInfo?.email}</p>
					</div>
					<div>
						<p class="legend">Address</p>
						<p>
							${this.contactInfo?.address?.street},
							${this.contactInfo?.address?.suite},
							${this.contactInfo?.address?.city},
							${this.contactInfo?.address?.zipcode}
						</p>
					</div>
				</div>
			</div>
		`;
	}
}

customElements.define('lit-contact-card', LitContactCard);
