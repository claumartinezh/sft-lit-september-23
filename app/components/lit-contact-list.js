import { LitElement, html, css } from 'lit-element';

export class LitContactList extends LitElement {
	static get properties() {
		return {
			contactsList: { type: Array },
			inputContact: { type: String },
		};
	}

	constructor() {
		/**
		 * Ejercicio 4.1
		 * Crear un componente que recibe un array de objetos con la siguiente estructura:
		 * [{name: 'Contact One'}, ...]
		 *
		 * 1) Desplegar la lista
		 * 2) Incluir un input y botón para agregar contactos a la lista
		 * 3) Incluir un botón para borrar el último contacto de la lista
		 */
		super();
		this.contactsList = [];
		this.inputContact = '';
	}

	render() {
		return html`
			<div>
				<input
					name="input-contact"
					.value=${this.inputContact}
					@change="${this.inputContactChanged}"
				/>
				<button @click="${this.addContact}">Add contact</button>
				<button @click="${this.deleteContact}">Delete contact</button>
				<button @click="${this.shuffleList}">Shuffle</button>
			</div>
			<ul>
				${this.contactsList.map((contact) => html`<li>${contact.name}</li>`)}
			</ul>
		`;
	}

  /**
   * Handles input change
   * @param {Event} ev input change event
   */
	inputContactChanged(ev) {
		this.inputContact = ev.target.value;
	}

	/**
	 * Adds entered contact to list
	 */
	addContact() {
		if (this.inputContact) {
      // OPCION 1 (spread operator)
      this.contactsList = [...this.contactsList, { name: this.inputContact }]

      // OPCION 2
			// this.contactsList.push({ name: this.inputContact });

			this.inputContact = '';
		}
	}

	/**
	 * Deletes last contact from list
	 */
	deleteContact() {
    // OPCION 1 (spread operator)
    this.contactsList = [...this.contactsList.slice(0, -1)];

    // OPCION 2
		// this.contactsList = this.contactsList.slice(0, -1);
	}

  /**
   * Re-orders list in a random way
   */
	shuffleList() {
		this.contactsList = [...this.contactsList.sort((a, b) => 0.5 - Math.random())];

    // OPCION 2
		// this.contactsList = this.contactsList.sort((a, b) => 0.5 - Math.random());
		// this.requestUpdate();
	}
}

customElements.define('lit-contact-list', LitContactList);
