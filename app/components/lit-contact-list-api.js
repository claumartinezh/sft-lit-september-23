import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';

export class LitContactListApi extends LitElement {
	static get properties() {
		return {
			contactsList: { type: Array },
			inputContact: { type: String },
			url: { type: String },
		};
	}

	static get styles() {
		return [
      sharedStyles,
      css`
			:host {
				display: flex;
				flex-flow: column;
        align-items: center;
				font-size: 15px;
			}
			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}
			.contacts-list {
				list-style: none;
        width: 100%;
			}
			.contacts-list li {
				background-color: #d4edfc;
				padding: 10px;
				display: flex;
				justify-content: space-between;
        align-items: center;
			}
			.contacts-list li:hover {
				background-color: #2dcccd;
			}
			.contacts-list li:focus {
				background-color: #028484;
			}
			.delete-x-button {
				background-color: #da3851;
        padding: 8px 16px;
			}
			.add-contact-section {
				display: flex;
				gap: 10px;
			}
			.primary {
				background-color: #028484;
			}
			.secondary {
				background-color: #1464a5;
			}
		`]
	}

	constructor() {
		/**
		 * Ejercicio 4.2
		 * Crear un componente que recibe un array de objetos desde un API 'https://jsonplaceholder.typicode.com/users'
		 * con la siguiente estructura:
		 * [{name: 'Contact One'}, ...]
		 *
		 * 1) Desplegar la lista
		 * 2) Incluir un input y botón para agregar contactos a la lista
		 * extra a) Incluir un botón para borrar el último contacto de la lista
		 * extra b) Incluir un botón para hacer suffle a los contactos
		 */
		super();
		this.contactsList = [];
		this.inputContact = '';
		this.url = 'https://jsonplaceholder.typicode.com/users';
	}

	firstUpdated() {
		this.getContacts();
	}

	render() {
		return html`
			<div class="add-contact-section">
				<input
          class="input-default"
					name="input-contact"
					value=${this.inputContact}
					@change="${this.inputContactChanged}"
				/>
				<button class="button-default primary" @click="${this.addContact}">
					Add contact
				</button>
				<button
					class="button-default primary"
					@click="${this.deleteLastContact}"
				>
					Delete last contact
				</button>
				<button class="button-default secondary" @click="${this.shuffleList}">
					Shuffle
				</button>
			</div>
			<ul class="contacts-list">
				${this.contactsList.map(
					(contact) =>
						html` <li
							@click=${() => this.contactClicked(contact)}
							tabindex=${contact.id}
						>
							<!-- 1) le pasamos como param al método contactClicked el evento -->
							${contact.name}
							<button
								class="button-default delete-x-button"
								@click=${() => this.deleteClickedContact(contact)}
							>
								X
							</button>
						</li>`
				)}
			</ul>
		`;
	}

	/**
	 * Handles input change
	 * @param {Event} ev input change event
	 */
	inputContactChanged(ev) {
		this.inputContact = ev.target.value;
	}

	/**
	 * Adds entered contact to list
	 */
	addContact() {
		if (this.inputContact) {
			this.contactsList.push({
				name: this.inputContact,
				username: 'Default username',
				email: 'defaultmail@mail.com',
				address: {
					street: 'Default Street',
					suite: '000',
					city: 'Default',
					zipcode: '0000-0000',
				},
			});
			this.inputContact = '';
		}
	}

	/**
	 * Deletes last contact from list
	 */
	deleteContact() {
		// OPCION 1 (spread operator)
		this.contactsList = [...this.contactsList.slice(0, -1)];

		// OPCION 2
		// this.contactsList = this.contactsList.slice(0, -1);
	}

	/**
	 * Re-orders list in a random way
	 */
	shuffleList() {
		this.contactsList = [
			...this.contactsList.sort((a, b) => 0.5 - Math.random()),
		];

		// OPCION 2
		// this.contactsList = this.contactsList.sort((a, b) => 0.5 - Math.random());
		// this.requestUpdate();
	}

	/**
	 * Calls url to get contacts list
	 */
	getContacts() {
		fetch(this.url)
			.then((response) => response.json())
			.then((data) => {
				this.contactsList = data;
			})
			.catch((err) => {
				console.log('There was an error on fetch', err);
			});
	}

	/**
	 * Updates contact styles and dispatches an event with contact info.
	 * @param {Event} ev click event
	 * @param {Object} contact selected contact object
	 */
	contactClicked(contact) {
		// La manera de comunicar datos hacia fuera del componente es lanzando un evento.
		// Lo que se quiera enviar hacia fuera irá dentro del campo 'detail'.
		// En este caso enviamos el objeto contact ya que tiene todos los datos del contacto seleccionado.
		this.dispatchEvent(
			new CustomEvent('contact-clicked', {
				detail: contact,
				// Se pone a true bubbles y composed si queremos que el evento se emita hacia arriba en el shadow DOM (componente padre, abuelo, etc)
				// bubbles: true,
				// composed: true,
			})
		);
	}

	/**
	 * Deletes clicked contact from the list
	 * @param {Object} contact clicked contact to delete
	 */
	deleteClickedContact(contact) {
		this.contactsList = [
			...this.contactsList.filter((item) => item !== contact),
		];
	}
}

customElements.define('lit-contact-list-api', LitContactListApi);
