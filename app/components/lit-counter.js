import { LitElement, html, css } from 'lit-element';

export class LitCounter extends LitElement {
	static get properties() {
		return {
			/**
			 * Propiedad contador
			 */
			counter: { type: Number },
		};
	}

	static get styles() {
		return css`
			:host {
				display: block;
				box-sizing: border-box;
				font-size: 20px;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}
		`;
	}

	constructor() {
		/**
		 * Ejercicio 2
		 * Crear un componente que muestre un botón y un texto que muestre el número de clicks realizados.
		 *
		 */
		super();

		// Inicializamos la propiedad a 0
		this.counter = 0;
	}

	render() {
		return html`
			<!-- Renderizamos un texto con la propiedad que es dinámica -->
			<p>Llevas ${this.counter} clicks!</p>

			<!-- Llamamos al método increment que modifica esa propiedad -->
			<button @click="${this.increment}">Suma al contador</button>
		`;
	}

	/**
	 * Método que incrementa el contador +1
	 */
	increment() {
		this.counter++;
	}
}

customElements.define('lit-counter', LitCounter);
