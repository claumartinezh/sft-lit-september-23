import { LitElement, html, css } from 'lit-element';
import { sharedStyles } from '../../sft-lit-element-styles';

export class LitHelloName extends LitElement {
	static get properties() {
		return {
			/**
			 * Propiedad nombre
			 */
			name: { type: String },
		};
	}

	constructor() {
		/**
		 * Ejercicio 1
		 * Crear un componente que muestre un saludo y pueda modificar el nombre mediante un atributo.
		 *
		 * Estado inicial:
		 *  Hello, <put name here>
		 *
		 * Resultado:
		 *  Hello, Sr. Thomson
		 */
		super();

		// Inicializamos la propiedad para que por defecto tenga el valor '<put name here>'
		this.name = '<put name here>';
	}

	static get styles() {
		return [
    sharedStyles,
    css`
			:host {
				display: block;
				box-sizing: border-box;
				color: grey;
				font-size: 20px;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			.name {
				color: #2dcccd;
			}
		`];
	}

	render() {
		return html`
			<!-- Renderizamos un texto con la propiedad-->
			Hello, <span class="name">${this.name}</span>
		`;
	}
}

customElements.define('lit-hello-name', LitHelloName);
