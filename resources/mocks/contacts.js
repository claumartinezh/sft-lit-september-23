export default [
  {
    id: '00',
    name: 'Spongebob Squarepants',
    username: 'spongepants',
    email: 'spongebob01@undersea.com',
    address: {
      street: 'Pineapple St.',
      suite: 1,
      city: 'Undersea',
      zipcode: '1111-1010',
    },
  },
  {
    id: '01',
    name: 'Ned Flanders',
    username: 'Lefthanded55',
    email: 'neddyflanders@simpsons.com',
    address: {
      street: 'Evergreen Terrace',
      suite: 111,
      city: 'Springfield',
      zipcode: '01882-0000',
    },
  },
  {
    id: '02',
    name: 'Ash Ketchum',
    username: 'pokemaniak001',
    email: 'ashandpikachu@pokedex.com',
    address: {
      street: 'Palet Town',
      suite: 2,
      city: 'Kanto',
      zipcode: '62626-3310',
    },
  },
];
