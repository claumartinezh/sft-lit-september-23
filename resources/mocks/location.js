export default {
  street: {
    number: 742,
    name: 'Evergreen Terrace',
  },
  city: 'Springfield',
  state: 'Oregon',
  country: 'United States',
};