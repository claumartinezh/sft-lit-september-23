export default {
	rank: 2,
	title: "(I Can't Get No) Satisfaction",
	artist: 'The Rolling Stones',
	album: 'Out of Our Heads',
	year: '1965',
	duration: '03:42',
	url: 'https://www.youtube.com/watch?v=MSSxnv1_J2g',
};